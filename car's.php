<?php
Class Car{
    public $sub;
    public $tank;
    private $savedfuel;
    public function fill($fillamound)
    {
        $this->tank = $fillamound;
        return $this;
    }
    public function ride($distanceRide){
        $this->savedfuel = $distanceRide/50;
        $this->sub = $this->tank - $this->savedfuel;
        return $this;
    }

}
$bmw = new Car();
echo $bmw -> fill(40) -> ride(50)->tank;
?>